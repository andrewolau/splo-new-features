
# README

Confluence page:  
https://woolworthsdigital.atlassian.net/wiki/spaces/DGDMS/pages/25721323227/New+features+for+SPLO


### What is this repository for?

* Quick summary
Exploratory work and offline testing for potential new SPLO features.  

* Version  
0.1

# Questions

- Is this the standard procedure for offline evaluation?
Doesn't appear to be a standard procedure. In Big W, they refit the AKL model and see if the features appears in the top 200 (they target 135 features).
- In Stu's data, is the field **item_score** the CF score?
Looks like it
- What’s the standard comparison metric?
Ideally something like redemption rate as that's what the CM's seem to care about. Note that redemption rate for Big W is at the sub-cat level, due to the large 100k product space
Otherwise recall @ K seems common (BWS uses this).
Usual binary classification AKL diagnostics are generally not used, it's more of a diagnostic.

- What is the data
?2 year historical transaction
training period maybe 1 year 50 weeks
validation ~ two week within the last year
test ~ some weeks outside of 1 year
^ data for WX/YH's analysis came from Stu

- What’s the CV fold in the AKL config file?
Data generated from Stu, custom CV fold splits.  Something to do with AKL 0.6 and manual data splitting done before AKL. WX/YH didn't touch data

- Which model was best?
Only the recommended embeddings from Weixing were tested.
- Why not merge into the main SPLO pipeline as a branch and run the BB pipeline there?
A/B testing not done. Offline testing never entirely finished due to a chapter change. Uplift was small ~0.5% in the recall @ K metrics for a lot of the curve.
- For the NLP model, are x_01, ... the BERT embeddings?
Looks like it

wx-auto-ml/weixing/chapter_project/splo_49/prod_embed_sentence1/splo-49-s1-l8h128v4-embed-20211020/diagnosis/


**Current scoring practice for PLO sends**
```
for each i in crn:
	for each j product:
		score(i, j)

# then rank by desc score
```
**However, in Ying's offline test**

```
for i crn:
	for j crn_i_products:
		score(i, j)
# limited product space from product space
```

Big W testing
All positive (eg bought it) CRN/product pairs and add in random product for each CRN as negative sample



### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin  
Andrew Lau
* Other community or team contact  
Stuart Hatzioannou