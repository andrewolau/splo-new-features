#!/bin/bash
# create the environment in the directory for easier access
echo "INFO: creating conda environment"
conda create -n splo-new-features -y

echo "INFO: activating conda env"
source activate splo-new-features

echo "INFO: installing from requirements.txt"
pip install -r requirements.txt

# echo "INFO: adding conda env to jupyter notebook envs"
python -m ipykernel install --user --name splo-new-features --display-name "Python (splo-new-features)"
